package com.testeoF.module;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;



@Entity
@Table(name = "mensaje")
@EntityListeners(AuditingEntityListener.class)
public class Mensaje {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "codigo_m", length=5 ,nullable = false)
	private String codigo;
	
	@Column(name = "texto_m", length=20 ,nullable = false)
	private String texto;

	public Mensaje() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Mensaje(long id, String codigo, String texto) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.texto = texto;
	}

	public long getId() {
		return id;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getTexto() {
		return texto;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}


}
