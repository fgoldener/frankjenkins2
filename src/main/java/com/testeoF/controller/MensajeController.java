package com.testeoF.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.testeoF.exception.ResourceNotFoundException;
import com.testeoF.module.Mensaje;
import com.testeoF.repo.MensajeRepository;

@CrossOrigin(origins = "http://192.168.99.100:30823")
@RestController
@RequestMapping("/api/v1")
public class MensajeController {
	@Autowired
	private MensajeRepository mensajeRepository;
	
	@GetMapping("/mensajes")
	public List<Mensaje> getAllMensajes(){
		return mensajeRepository.findAll();
	}
	
	@GetMapping("/mensajes/{id}")
	public ResponseEntity<Mensaje> getMensajeById(@PathVariable(value="id") Long mensajeId)
	throws ResourceNotFoundException {
		Mensaje mensaje =mensajeRepository.findById(mensajeId)
				.orElseThrow(() -> new ResourceNotFoundException("Mensaje "+mensajeId+" no encontrado"));
		return ResponseEntity.ok().body(mensaje);
	}
	
	@PostMapping("/mensajes")
	public Mensaje createMensaje(@Valid @RequestBody Mensaje mensaje) {
		return mensajeRepository.save(mensaje);
	}
	
	@PutMapping("/mensajes/{id}")
	public ResponseEntity<Mensaje> updateMensaje(@PathVariable(value="id") Long mensajeId, @Valid @RequestBody Mensaje mensajeDetails)
	throws ResourceNotFoundException{
		Mensaje mensaje = mensajeRepository.findById(mensajeId)
				.orElseThrow(() ->new ResourceNotFoundException("Mensaje "+mensajeId+" no encontrado"));
		
		mensaje.setCodigo(mensajeDetails.getCodigo());
		mensaje.setTexto(mensajeDetails.getTexto());
		
		final Mensaje updateMensaje = mensajeRepository.save(mensaje);
		return ResponseEntity.ok(updateMensaje);
	}
	
	@DeleteMapping("/mensajes/{id}")
	public Map<String, Boolean> deleteMensaje(@PathVariable(value="id") Long mensajeId)throws ResourceNotFoundException{
		Mensaje mensaje= mensajeRepository.findById(mensajeId)
				.orElseThrow(() -> new ResourceNotFoundException("Mensaje "+mensajeId+" no encontrado"));
		
		mensajeRepository.delete(mensaje);
		Map<String, Boolean> response = new HashMap<>();
		response.put("eliminado", Boolean.TRUE);
		return response;
	}
	
	

}
