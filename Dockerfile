FROM openjdk:8
EXPOSE 8082
ADD target/appcrud-springboot.jar appcrud-springboot.jar
ENTRYPOINT ["java","-jar","appcrud-springboot.jar"]
